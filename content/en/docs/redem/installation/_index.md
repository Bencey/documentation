---
title: "Installation"
weight: 5
---

### Guide

<div style="display: flex;">
<div style="width: 50%; display: inline-block; float: left">
{{% alert title="Notice" color="primary" %}}
Click [here](https://m.do.co/c/8329d881273b) for **$100** of free credit at Digital Ocean and support our development
{{% /alert %}}
</div>
<div style="width: 50%; display: inline-block;">
{{% alert title="Alert" color="danger" %}}
This guide assumes you have a working FXServer, to get that setup please take a look [here](https://docs.fivem.net/docs/server-manual/setting-up-a-server/)
{{% /alert %}}
</div>
</div>

#### Downloads

First you need to download the following resources (middle mouseclick to open them in a new tab)

* [RedEM](https://github.com/kanersps/redem)
* [RedEM: Roleplay](https://github.com/RedEM-RP/redem_roleplay)
* [MySQL-Async](https://github.com/amakuu/mysql-async-temporary)
* [esplugin_mysql](https://github.com/RedEM-RP/esplugin_mysql)
* [RedEM: RP Identity](https://github.com/RedEM-RP/redemrp_identity)
* [RedEM: RP Respawn](https://github.com/RedEM-RP/redemrp_respawn)

#### Steps
1. Make sure you've read the notices above
2. When you downloaded the above download and move/extract the files to your resources folder
3. Make sure that the name of `fivem-mysql-async` is `mysql-async`
4. Setup the following in your server configuration file(be sure to change the MySQL connection string options):
```
set es_enableCustomData 1
set mysql_connection_string "server=127.0.0.1;uid=root;password=1202;database=redemrp"
 
ensure esplugin_mysql
ensure mysql-async
ensure redem
ensure redem_roleplay
ensure redemrp_identity
 
# Other addons belows (provided an example)
ensure redemrp_respawn #example
```
5. Create a database named "redemrp" in your MySQL server (all content will be automatically created on first run of your server)
6. Start your RedM server
7. Magic!