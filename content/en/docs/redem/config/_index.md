---
title: "Configuration"
date: 2020-06-14T19:32:20+02:00
draft: true
---

### Available configuration option
{{% alert title="Notice" color="primary" %}}
These can be set in your server configuration file
{{% /alert %}}

These are all the options and their default values
``` lua
-- Draw the player's server ID on the screen
set redemrp_showid 1

-- Enable or disable Admin Commands
set redemrp_admincommands 1

-- Reveal all of the map for the player
set redemrp_revealmap 1

-- Draw the coordinates on the screen
set redemrp_showcoords 1

-- Draw the players level on the screen
set redemrp_showlevel 1

-- Enable or disable PvP
set redemrp_enablepvp 1
```