---
title: "Events"
---

EssentialMode features many handy events to help you make your resources amazing. All of them are listed here.

### Triggerable Events

```lua
-- es:addCommand
TriggerEvent('es:addCommand', 'command-here-without-slash', function(source, args, user) end, {help here})
 
-- es:addAdminCommand
TriggerEvent('es:addAdminCommand', 'command-here-without-slash', permission-level, function(source, args, user)
     -- Has permission
end, function(source, args, user)
     -- Doesn't have permission
end, {help here})
 
-- es:getPlayerFromId
TriggerEvent('es:getPlayerFromId', source, function(user)
     -- The user object is either nil or the loaded user.
end)
 
-- es:setPlayerData
TriggerEvent('es:setPlayerData', source, key, value, function(message, success) end)
 
-- es:setPlayerDataId
TriggerEvent('es:setPlayerDataId', id, key, value, function(message, success) end)
 
-- es:getPlayers
TriggerEvent('es:getPlayers', function(players) end)
 
-- es:setDefaultSettings
TriggerEvent("es:setDefaultSettings", {})
 
-- es:setSessionSetting
TriggerEvent("es:setSessionSetting", key, value)
 
-- es:getSessionSetting
TriggerEvent("es:getSessionSetting", key, callback)
 
-- es:addGroup
TriggerEvent("es:addGroup", "groupname", "inherits", "ACL Group")
 
-- es:getAllGroups
TriggerEvent("es:getAllGroups", function(groups) end)
 
-- es:addGroupCommand
TriggerEvent("es:addGroupCommand", command, group, callback, callbackfailed, {help here})
```

### Listening events

```lua
-- es:firstSpawn
AddEventHandler('es:firstSpawn', function(source, user) end)
 
-- es:playerLoaded
AddEventHandler('es:playerLoaded', function(source, user) end)
 
-- es:newPlayerLoaded
AddEventHandler('es:newPlayerLoaded', function(source, user) end)
 
-- es:userCommandRan
AddEventHandler('es:userCommandRan', function(source, command_args, user) end)
 
-- es:commandRan
AddEventHandler('es:commandRan', function(source, command_args, user) end)
 
-- es:adminCommandRan
AddEventHandler('es:adminCommandRan', function(source, command_args, user) end)
 
-- es:invalidCommandHandler
AddEventHandler('es:invalidCommandHandler', function(source, command_args, user) end)
 
-- es:adminCommandFailed
AddEventHandler('es:adminCommandFailed', function(source, command_args, user) end)
 
-- es:chatMessage
AddEventHandler('es:chatMessage', function(source, command_args, user) end)
 
-- es:playerDropped
AddEventHandler('es:playerDropped', function(user) end)
```