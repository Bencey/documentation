---
title: "Installation"
weight: 1
---

<div style="display: flex;">
<div style="width: 50%; display: inline-block; float: left">
{{% alert title="Notice" color="primary" %}}
Click [here](https://m.do.co/c/8329d881273b) for **$100** of free credit at Digital Ocean (you can host your server here) and support our development
{{% /alert %}}
</div>
<div style="width: 50%; display: inline-block;">
{{% alert title="Zap-Hosting" color="primary" %}}
Zap-Hosting offers users of EssentialMode a lifetime 10% discount, if you'd like to have them setup and manage your server then click [here](https://zap-hosting.com/EssentialMode) to start! (The discount code: `kanersps-a-2529`)
{{% /alert %}}
</div>
</div>

### Basic installation

1. Download EssentialMode from here
2. Extract it into your resources folder and rename the folder to "essentialmode"
3. Append "start essentialmode" to your server configuration, and all addons after that
4. Also make sure to set the following in your server.cfg file (before "start essentialmode")

```
add_ace resource.essentialmode command.sets allow
add_ace resource.essentialmode command.add_principal allow
add_ace resource.essentialmode command.add_ace allow
```

5. Start the server and everything should work fine

### MySQL
Want to use MySQL for your server? Take a look [here](../database)

### User Interface
{{% alert title="Notice" color="primary" %}}
If you're using ExtendedMode you will not need to install this UI component, it is intergrated into ExtendedMode.
{{% /alert %}}

If you want to utilize EssentialMode's UI component you require to install [this](https://github.com/kanersps/es_ui)

### Addons

If you would like to install additional resources for EssentialMode you require to start them after EssentialMode itself. An example can be found here:
```
start essentialmode
start es_admin2
start es_otherresource
```